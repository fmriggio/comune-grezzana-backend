	</div>

	<footer>
		<p>Factotum ©2013  / Written by <a href="http://www.filippomatteoriggio.it">Filippo Matteo Riggio</a></p>
	</footer>

<?php if (count($footerJS) > 0) { ?>
	
	<?php foreach ($footerJS as $file) { ?>

			<script src="<?php echo $file; ?>" type="text/javascript"></script>
	<?php } ?>

<?php } ?>

	</body>

</html>